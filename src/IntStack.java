import java.util.LinkedList;
import java.util.List;

public class IntStack {

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }
   private  LinkedList<Integer> stack;
   public IntStack() {
      // TODO!!! Your constructor here!
      stack = new LinkedList<Integer>();
   }

   public IntStack(LinkedList<Integer> stack) {
      this.stack = stack;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LinkedList<Integer> clonedList = new LinkedList<Integer>(stack);
      return new IntStack(clonedList); // TODO!!! Your code here!
   }

   public boolean stEmpty() {
      if (stack.isEmpty()){
         return true;
      }
      return false; // TODO!!! Your code here!
   }

   public void push (int a) {
      // TODO!!! Your code here!
      stack.add(a);
   }

   public int pop() {
      int topInt = stack.remove(stack.size()-1);
      return topInt; // TODO!!! Your code here!
   } // pop

   public void op (String s) {
      // TODO!!!
      int topElement = this.pop();
      int nextTopElement = this.pop();
      if (s.equals("+")){
         stack.add(topElement + nextTopElement);
      }else if(s.equals("-")){
         stack.add(nextTopElement - topElement);
      }else if(s.equals("*")){
         stack.add(topElement * nextTopElement);
      }else{
         stack.add( nextTopElement / topElement);
      }
   }
  
   public int tos() {
      return stack.get(stack.size()-1);
       // TODO!!! Your code here!
   }

   @Override
   public boolean equals (Object o) {
      if (this == o) return true;
      if (o == null) return false;
      if (!(o instanceof IntStack) ) return false;
      IntStack oCasted = (IntStack) o;
      List<Integer> l1 = this.stack;
      List<Integer> l2 = oCasted.stack;
      if (l1.size() != l2.size()) return false;

      for(int a = 0; a < l1.size(); a++){
         if(!l1.get(a).equals(l2.get(a))){
            return false;
         }
      }

      return true; // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      return stack.toString();
       // TODO!!! Your code here!
   }

   public static int interpret (String pol) {
      IntStack m = new IntStack();

      String[] elements = pol.split(" ");
      for(String element: elements){
         element = element.replaceAll("\t", "");
         if(element.equals(" ") || element.equals("") ){

         }else if (element.equals("+")){
            m.op("+");
         }else if(element.equals("-")){
            m.op("-");
         }else if(element.equals("*")){
            m.op("*");
         }else if(element.equals("/")){
            m.op("/");
         }else if(!element.matches("-?\\d+(\\.\\d+)?")){
            throw new RuntimeException("Expression is not correct");
         }else  {
            m.push(Integer.parseInt(element));
         }
      }

      if (m.stack.size() > 1){
         throw new RuntimeException("Expression is not correct");
      }
      return m.tos(); // TODO!!! Your code here!
   }

}

